CC=gcc
CFLAGS=-I.
OBJ=hello-world.o

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

hello-world: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	rm -rf *.o hello-world
